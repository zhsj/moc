######################################################################
# Description:   Debian terms for moc
# Author:        Elimar Riesebieter <riesebie@lxtec.de>
# Created:       Sun, 26 Jun 2005 16:25:26 +0200
# Last modified: Fri, 22 Apr 2011 12:14:12 +0200
# File:          /usr/share/doc/moc/README.Debian
######################################################################

Music On Console is a console audio player for LINUX/UNIX designed to
be powerful and easy to use

You just need to select a file from some directory using the menu
similar to Midnight Commander, and MOC will start playing all files in
this directory beginning from the chosen file. There is no need to
create playlists like in other players.

If you want to combine some files from one or few directories on one
playlist, you can do this. The playlist will be remembered between
runs or you can save it as an m3u file to load it whenever you want.

Need the console where MOC is running for more important things? Need
to close the X terminal emulator? You don't have to stop playing -
just press q and the interface will be detached leaving the server
running. You can attach it later, or you can attach one interface in
the console, and another in the X terminal emulator, no need to switch
just to play another file.

MOC plays smoothly, regardless of system or I/O load because it uses
the output buffer in a separate thread. It doesn't cause gaps between
files, because the next file to be played is precached while playing
the current file.

Supported file formats are: MP3, Musepack, OGG Vorbis, WAVE, FLAC and
new formats support is under development.

Since moc-2.2.2+2.3.0-alpha1 there are some additional features
available. Please check changelog.gz and changelog.Debian.gz.
It is recommended to diff your $HOME/.moc/config with
examples/config.example.gz to check new config-variables.
Same for 2.3.0 ;-)

Please notice to start moc with the command:
$ mocp
as the binary moc generates Qt meta object support code ;-)

The manpage is accommodated to the Debian doc behavior.

----------------------------------------------------------------------

With version 2.3.3+2.4.0alpha2-1 I introduced a separated
moc-ffmpeg-plugin binary. You need this installed, if you want to play
sound files out of the ffmpeg family. In moc the ffmpeg-libs are
linked static, because at the moment we do not have dynamic linked
ones out of the ffmpeg package. The plugin itself is around 2.5 MB, so
it makes sense to keep the moc binary small.

$ apt-get install moc-ffmpeg-plugin

----------------------------------------------------------------------

With version 2.4.0-2 I introduced ModPlug support by Hendrik Iben's
plugin from http://www.tzi.de/~hiben/moc/. Please check
/usr/share/doc/examples/config.example.gz for possible options playing
mod files ;)
----------------------------------------------------------------------

Since version 2.5.0~alpha2 wavpack and sidplay2 are supported. You can
find samples:
http://www.rarewares.org/wavpack/test_suite.zip
and some SID files:
http://www.hvsc.c64.org/Downloads/HVSC_Update_47.zip
----------------------------------------------------------------------

If you get:
FATAL_ERROR: Can't send() int to the server
Try:
$ rm -rf $HOME/.moc/cache
and restart moc
----------------------------------------------------------------------

Have fun ;-)

Elimar Riesebieter <riesebie@lxtec.de>

vim:tw=70:ts=4
